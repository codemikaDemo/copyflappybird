import pygame
from random import randrange
pygame.init()

# Frame
bg_image = pygame.image.load('background.jpg')
window_width = 1440
window_height = 950

# Bird
bird_image = pygame.image.load('bird.png')
bird_image_small = pygame.transform.scale(bird_image, (40, 40))
bird_width = bird_image.get_width()
bird_height = bird_image.get_height()
bird_fly = 0.02 * window_height
bird_down = 0.003 * window_height
bird_life = 10

# Wall
wall_image = pygame.image.load('wall.png')
wall_h1 = 880
wall_h2 = 1120
wall_width = wall_image.get_width()
wall_height = wall_image.get_height()
wall_speed = 0.002 * window_width
wall_delta = 700