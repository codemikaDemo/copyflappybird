from Wall import *
from Bird import *

window = pygame.display.set_mode((window_width, window_height))
background = bg_image
bird = Bird()
walls = []
font = pygame.font.SysFont('serif', 30)
bally = 0
panel = pygame.Surface((window_width, 50))
panel.fill(pygame.Color('black'))

hide = pygame.image.load('hide.png')
hide = pygame.transform.scale(hide, (40, 40))

turbo = pygame.image.load('turbo.png')
turbo = pygame.transform.scale(turbo, (40, 40))

chit = font.render('cheats: ', True, pygame.Color('white'))
cheats = []
for i in range(window_width // wall_delta):
    walls.append(Wall(i))

while True:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            exit()

    key = pygame.key.get_pressed()
    if key[pygame.K_SPACE]: bird.jump()
    if key[pygame.K_LEFT]: bird.left()
    if key[pygame.K_RIGHT]: bird.right()
    if key[pygame.K_q]:
        bird.hide()
        if cheats.count(hide) == 0:
            cheats.append(hide)
    if key[pygame.K_c]:
        bird.start()
        if cheats.count(hide) > 0:
            cheats.remove(hide)
    if key[pygame.K_t]:
        if cheats.count(turbo) == 0:
            cheats.append(turbo)
        for wall in walls:
            wall.turbo()
        bird.turbo()
    if key[pygame.K_y]:
        if cheats.count(turbo) > 0:
            cheats.remove(turbo)
        for wall in walls:
            wall.turbo_stop()
        bird.turbo_stop()

    bird.fly()
    bally += 0.01
    text = font.render('Баллы: ' + str(int(bally)), True, pygame.Color('white'))
    window.blit(background, (0, 0))
    window.blit(bird.img, (bird.x, bird.y))
    for wall in walls:
        if bird.check(wall):
            if bird.lifes == 1: exit()
            bird.start()
            walls.clear()
            for i in range(window_width // wall_delta):
                walls.append(Wall(i))
            bird.lifes -= 1
        wall.go()
        window.blit(wall.img, (wall.x, wall.y))
    window.blit(panel, (0, 0))
    window.blit(text, (5, 5))
    for i in range(bird.lifes):
        window.blit(bird_image_small, (150 + 40 * i, 5))
    if len(cheats) > 0:
        window.blit(chit, (175 + 40 * bird_life, 5))
        for i in range(len(cheats)):
            window.blit(cheats[i], (275 + 40 * (bird_life + i), 5))
    pygame.display.update()

pygame.quit()