from Settings import *

class Wall:
    x, y, speed = 0, 0, 0
    img = wall_image

    def __init__(self, i):
        self.x = window_width + wall_delta * i
        self.range_Y()
        self.speed = wall_speed

    def range_Y(self):
        # TODO поменять числа 800 и 100 на формулы, в зависимости от размеров
        self.y = randrange(-800, -100)

    def turbo(self):
        self.speed += 1

    def turbo_stop(self):
        self.speed = wall_speed

    def go(self):
        self.x -= self.speed
        if self.x < - wall_width:
            self.x = window_width
            self.range_Y()