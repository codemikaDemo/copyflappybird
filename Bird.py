from Settings import *

class Bird:
    x, y = 0, 0
    speed = bird_down
    img = bird_image
    lifes = bird_life

    def __init__(self):
        self.start()

    def start(self):
        self.x = window_width / 4
        self.y = window_height / 2

    def fly(self):
        self.y += self.speed
        if 0 <= self.x <= window_width and (self.y < -100 or self.y > window_height - 50):
            exit()

    def jump(self):
        self.y -= bird_fly

    def right(self):
        self.x += self.speed

    def left(self):
        self.x -= self.speed

    def hide(self):
        self.x = 2 * window_width

    def turbo(self):
        self.speed += 0.3

    def turbo_stop(self):
        self.speed = bird_down

    def check(self, wall):
        return (wall.x < self.x < wall.x + wall_width \
                or wall.x < self.x + bird_width < wall.x + wall_width) \
               and \
               (wall.y < self.y < wall.y + wall_h1 \
                or wall.y < self.y + bird_height < wall.y + wall_h1 \
                or wall.y + wall_h2 < self.y < wall.y + wall_height \
                or wall.y + wall_h2 < self.y + bird_height < wall.y + wall_height)